# Metodologia de software: SCRUM
    
![This is a alt text.](/imagen/foto.jpg "This is a sample image.")

#### La gestión de procesos y equipos para abordar proyectos complejos que requieren entornos dinámicos y cambiantes, por lo que exigen rapidez de resultados y flexibilidad. Se trata de una metodología de trabajo ágil que tiene varios objetivos, por ejemplo:

* Acelerar los procesos para satisfacer al cliente
* Actuar con rapidez ante los posibles cambios
* Realizar entregas periódicas del trabajo


### Caracteristicas

| Transparencia | Adaptacion | Inspeccion|
|:-------------:|:----------:|:---------:|
|Conocimiento común del estado del proyecto               | Ajustes para conseguir el objetivo            |El trabajo fluye y el equipo funciona


### Metodologia basada en 

* Flexibilidad 
* Factor humano
* Colaboración
* Asegura buenos resultados

### Fases de esta metodologia 

1. Inicio
1. Planificación y estimación
1. Implementación
1. Revisión y retrospectiva
1. Lanzamiento

### Ventajas de la metodología Scrum

* **Scrum es muy fácil de aprender**
* **Se agiliza el proceso, ya que la entrega de valor es muy frecuente.**


### Desventajas de la metodología Scrum 

* **Dificil implementación de las tareas**
* **No siempre se ofrece un trabajo de calidad**




